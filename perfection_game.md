# Perfection Game est une technique d'évaluation et d'amélioration.

### Évaluation

* on donne une note entre 1 et 10 (10 = parfait)
* on énumère *précisément* ce qu'on a aimé
* on énumère *précisément* ce qu'il faudrait faire pour qu'on accorde 10 

#### Échelle

* 1 : aucune valeur et je suis capable d'ajouter toute la valeur grâce à mes commentaires
* 10 : ne peut pas être amélioré et/ou je n'ai aucune idée d'amélioration 

### Interdits

* commentaires négatifs (les transformer en améliorations possibles ou se taire)
* mettre moins de 10 si on n'est pas capable de proposer des améliorations précises 

### Forme

	Je note ta prestation <n>

	j'ai aimé :
		- p,
		- q,
		- ...
	pour que je donne 10, il faut a, b, ..., z 
  

### Réception des évaluations

* une suggestion peut-être fausse
* la combinaison de toutes les suggestions peut être négative
* celui qui reçoit les évaluations doit faire le tri entre les suggestions et rejeter implicitement les retours qui n'apportent pas de valeur. 

[source]: Core protocols
[atelier]:[http://www.xpday.net/Xpday2005/PerfectionGame.html]
