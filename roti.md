Comment évaluer la valeur d'une réunion ? En le comparant à ce qui est rare et précieux: le temps.

En fin de réunion, on l'évalue avec le Retour sur Temps Investi (alias ROTI **R**eturn **O**f **T**ime **I**nvested) :

    1 : J'ai tout bonnement perdu mon temps, je n'ai pas vu l'intérêt de cette réunion.
    2 : J'ai bien vu un intérêt à cette réunion, mais ça ne valait pas le temps que j'y ai passé.
    3 : Cette réunion vaut le temps que j'y ai passé, mais pas plus, ni moins.
    4 : Cette réunion vaut plus que le temps que j'y ai passé.
    5 : Cette réunion vaut très largement le temps que j'y ai passé. 

Pour éviter les phénomènes de conformisme, inviter les votants à voter simultanément.

Le ROTI n'est pas :

* une notation de la prestation de l'animateur de la réunion
* une notation du niveau de confort éprouvé durant la réunion
* une arme pour "punir" un comportement qu'on n'apprécie pas 

