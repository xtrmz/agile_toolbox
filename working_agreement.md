Des règles de «bonne conduite» ont été proposées et acceptées par tous .

* chacun fait de son mieux, dans le temps qui lui était imparti avec les informations dont il disposait : pas de jugement, pas de raillerie ;
* parler en son nom, usage du "je" ;
* écoute et respect des autres : pas d'aparté, pas d'utilisation d'ordinateur portable, téléphones portables éteints ou en vibreur si nécessaire ;