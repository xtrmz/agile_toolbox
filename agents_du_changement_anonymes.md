## Opening

"définition" de la notion «Agents du Changement Anonymes»
Je reprends la description de la session de la conférence agile france 2012 animé par Raphael Pierquin

```
Vous aimez l'organisation qui vous emploie ?

Vous consacrez une énergie considérable à tenter de la faire changer ?

Parfois vous avez réussi ?

Souvent, vous avez échoué ?»

Si vous avez répondu "oui" à ces 4 questions, vous êtes probablement

ce que j'appelle "un agent du changement".
```

> tu pourrais choisir n'importe quel thème. Le tout étant que les gens qui se présentent s'y soient recnonnus.)
 
##  Présentation

Chacun se présente et raconte pourquoi il se considère agent du changement


> C'est optionnel. Quand j'ai peu de temps, je zappe carrément cette étape.
> Ca fait raler tout le monde, mais je persiste : les présentations ont moins
> de valeur que le reste de l'activité.


## Sélection d'un problème
Les gens qui souhaitent se faire aider, propose un problème à l'oral et le résume sur le tableau.

> Ne t'attend pas à ce que les gens résument leur problème à l'écrit, c'est trop difficile.
> Par contre, toi, tu peux écrire 3 mots significatifs qui permettront aux gens de se
> souvenir du problème évoqué.

On vote pour le problème que l'on souhaite aborder

## Discussion circulaire autour du problème
### Le problème est explicité de nouveau par celui qui l'a soumis

C'est important qu'il formule une demande aux autres.
Par exemple "Je voudrais obtenir tel résultat, comment feriez vous si vous étiez à ma place."
 
### Les gens présents successivement pose une question (et une seule) et remercie une fois qu'ils ont leur réponse.

Précise que ce sont des questions "de clarification". Elles appellent des informations supplémentaires sur la situation actuelle ou ce qui a déjà été fait.
"Combien ..." , "As-tu déjà ..." sont des bons débuts de questions de clarification.
"Pourquoi n'as tu pas fait ..." n'est probablement pas une question de clarification (mais une suggestion cachée, à postponer jusqu'au 2ème tour".

 
## "A ta place, je ...", plutôt que "Tu devrais ..." 

Toujours de manière circulaire, les gens donnent une idée sous la forme : "Si j'étais toi, ....", 
et celui qui a soumis le problème, remercie pour l'idée.

> Surtout, il ne peux pas réagir.
> A la fin du tour, tu peux lui demander ce qu'il compte faire maintenant qu'il a entendu les réponses. Mais ne l'invite surtout pas à expliquer en quoi les réponses n'était pas pertinentes.
 

## Tour de table sur l'atelier #

Si ton objectif est de présenter le format de l'atelier, c'est important de débriefer.
Si ton objectif est de partager des pratiques de manière ponctuelle, c'est moins le cas.
Si ton objectif, c'est de reproduire plusieurs fois cet atelier avec les même personnes, c'est important.
